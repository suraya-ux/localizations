* [Español](./es/Inicio)
* [Français](./de/Démarrage-rapide)
* [Nederlands](./nl/Aan-de-slag)
* [Русский](./ru/Введение)
* [Slovenščina](./sl/Pričetek)
* [Tagalog](./tl/Pagsisimula)
* [Українська](./uk/Вступ)
* [简体中文](./zh/起步)
* [English](./en/Getting-Started)
* [български език](./bg/Начало)
* [Deutsch](./de/Erste-Schritte)
* [Ελληνικά](./gr/Ξεκινώντας)
* [日本語](./ja/使い始める)
* [한국어](./ko/시작하기)